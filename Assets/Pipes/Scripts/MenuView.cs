﻿using UnityEngine;
using System.Collections;

public class MenuView : MonoBehaviour {

	public string title;
	public Texture2D background;
	public Texture2D scrollBar;
	public GameView game;
	public Font menuFont;

	TextAsset[] maps;
	GUIStyle titleStyle, levelStyle;
	int topLevel = 0;
	int currentLevel = 0;
	Vector2 scrollPosition = Vector2.zero;

	void Start () 
	{
		topLevel = PlayerPrefs.GetInt("level");

		maps = Resources.LoadAll<TextAsset>("Maps");
		StartCoroutine (Load ());
	}

	IEnumerator Load()
	{
		yield return new WaitForSeconds (0.5f);
		SetLevel (0);
	}

	public void SetLevel(int i)
	{
		gameObject.SetActive (false);
		game.CreateLevel(i + 1);
		currentLevel = i;
	}

	public void NextLevel ()
	{
		if(currentLevel == topLevel && topLevel < maps.Length)
		{
			topLevel++;
			PlayerPrefs.SetInt("level", topLevel);
		}
	}

	void Update()
	{
		// detect touch drag
		if(Input.touchCount > 0)
		{
			if (Input.touches[0].phase == TouchPhase.Moved)
				scrollPosition.y += Input.touches[0].deltaPosition.y;
		}
	}
}
